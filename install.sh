#!/bin/bash
# install with . ./install.sh

sudo apt update
sudo install git vim curl

# ROS install
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-melodic-desktop-full

# env variables
if [[ ${SHELL} == *"zsh"* ]]; then
  echo ". /opt/ros/melodic/setup.zsh" >> ~/.zshrc
  echo ". ~/catkin_ws/devel/setup.zsh" >> ~/.zshrc
  . ~/.zshrc
else
  echo ". /opt/ros/melodic/setup.bash" >> ~/.bashrc
  echo ". ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
  . ~/.bashrc
fi

# python3 tools and buildtools
sudo apt install -y python3 python3-dev python3-pip build-essential
sudo -H pip3 install rosdep rospkg rosinstall_generator rosinstall wstool vcstools catkin_tools catkin_pkg
sudo rosdep init
rosdep update
