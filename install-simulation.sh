#!/bin/bash

mkdir src
cd src
git clone https://gitlab.com/Yonder-Dynamics/misc/rover_simulation.git

cd rover_simulation
mkdir -p ~/.gazebo/models
cp -r .models/desert5 ~/.gazebo/models
cd ../..

sudo apt install ros-melodic-gazebo-ros-control ros-melodic-effort-controllers python3-tk
pip3 install scipy numpy

catkin build rover_simulation
if [[ ${SHELL} == *"zsh"* ]]; then
  . ~/.zshrc
else
  . ~/.bashrc
fi

sudo sed -i 's/_TIMEOUT_SIGINT  = 15.0/_TIMEOUT_SIGINT  = 2.0/' /opt/ros/melodic/lib/python2.7/dist-packages/roslaunch/nodeprocess.py
